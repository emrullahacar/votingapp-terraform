resource "aws_ecr_repository" "ecr_reponames" {
  for_each             = toset(var.repository_list)
  name                 = each.key
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}
