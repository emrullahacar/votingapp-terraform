#VPC
vpc_name="vpc-talent-emr"
vpc_cidr="10.0.0.0/16"
vpc_azs_list=["us-east-1a", "us-east-1b"]
private_subnet_list=["10.0.10.0/24"]
public_subnet_list=["10.0.100.0/24"]
enable_nat_gateway=true
enable_vpn_gateway=false

#EC2-K8S
cluster_name="cluster-talent-emr"
allowed_ssh_cidr_blocks=["0.0.0.0/0"]
allowed_k8s_cidr_blocks=["0.0.0.0/0"]

master_instance_type="t3.large"
tags= {"Talent" = "741796270453"}
root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 20
    },
  ]