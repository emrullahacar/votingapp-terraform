## Terraform Repository to create Kubernetes cluster in an EC2 via Kubeadm

## Components

- 1 VPC
- 1 Public Subnet
- 1 Private Subnet
- 1 EC2 instance in Public subnet
- 3 ECR repository
- Security Groups for EC2 instances
- 1 Elastic IP
- 1 Key Pair


## Usage

Firstly, fill provider.tf file via access key and secret key, or you can provide a profile name as well.

Create a file as terraform.tfvars and put variables in this file.

After that;

```
$ terraform init
$ terraform plan
$ terraform apply
```

## Architecture

![architecture](/img/architecture.png)