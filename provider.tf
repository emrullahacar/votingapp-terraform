terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  # The security credentials for AWS Account A.
  access_key = "xxxxx"
  secret_key = "xxxxxx"
  region     = "us-east-1"


  assume_role {
    # The role ARN within Account B to AssumeRole into. Created in step 1.
    role_arn = "arn:aws:iam::130575395405:role/talent_role"
  }
}
